import { component, React, Style } from 'local/react/component'
import { Icon } from 'local/react-ui/icons'

export const defaultEmptyStateStyle: Style = {
  textAlign: 'center',
  color: '#606060'
}

export const defaultEmptyStateIconStyle: Style = {
  display: 'block',
  fontSize: '200%',
  marginBottom: '0.5em'
}

export const EmptyState = component.children
  .props<{ style?: Style; iconStyle?: Style; iconName?: string }>({
    iconStyle: defaultEmptyStateIconStyle,
    style: defaultEmptyStateStyle
  })
  .render(({ style, iconStyle, iconName = 'info-circle', children }) => {
    return (
      <div style={style}>
        <Icon name={iconName} style={iconStyle} />
        {children}
      </div>
    )
  })
